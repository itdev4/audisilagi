<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if ($_SESSION['status_login']== "peserta"){
		header("location:index.php?pesan=salah");
	}
?>
<!DOCTYPE html>
<html>
<?php
	session_start();
		include('config.php');
	
		$cari_p= $_POST['cari_p'];
		$q = mysqli_query($konek, "SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian WHERE tbl_siswa.noujian='$cari_p'")or die(mysqli_eror($konek));
		$data_peserta= mysqli_fetch_object($q);

	?>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<?php
	
		include('config.php');
	
		$cari_p= $_POST['cari_p'];
		$q = mysqli_query($konek, "SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian WHERE tbl_siswa.noujian='$cari_p'")or die(mysqli_eror($konek));
		$data_peserta= mysqli_fetch_object($q);

	?>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_admin_cari.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><a  class="active" href="session_admin_cari.php?"><em class="fa fa-dashboard">&nbsp;</em> Penilaian Audisi </a></li>
			<li><a href="session_admin_addPeserta.php?"><em class="fa fa-calendar">&nbsp;</em> Tambah Peserta </a></li>
			<li><a href="session_admin_peserta.php?"><em class="fa fa-toggle-off">&nbsp;</em> Data Peserta</a></li>
			<li><a href="session_admin_audisi.php?"><em class="fa fa-calendar">&nbsp;</em> Data Audisi</a></li>
			<!--/.<li><a href="import_data.php?"><em class="fa fa-calendar">&nbsp;</em> Import Data</a></li> -->
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
	
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</li>
				<li class="active">Penilaian Audisi</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Penilaian Audisi :<?=$data_peserta->noujian?></h1>
			</div>
		</div><!--/.row-->
		

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
				<!--/.> PANEL  Data Peserta audisi-->
					<div class="panel-heading">
						Data Peserta Audisi 
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								<div class="form-group">
									<label class="col-md-2 control-label" for="name">Nama</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->name?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="tgllhr">Tanggal Lahir </label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->tgllhr?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="jurusan">Program Studi</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->prodi?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="name">Waktu Audisi</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" > 13 Oktober 2020 (10.30 - 11.00) </label>
									</div>
								</div>
							</fieldset>
						</form>
						
					</div><!--/.col-->
				</div>
			</div>
		</div><!--/.row-->
		<div>
				<div class="panel panel-default">
					<div class="panel-heading">DATA AUDISI
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
				</div>
					<div class="panel-body">
						<div class="col-md-6">
						<form role="form" method="POST" action="" >
									<div class="form-group">
										<label>Nomor Ujian</label>
										<input class="form-control" name="noujian" value="<?=$data_peserta->noujian?>" >
									</div>
									<div class="form-group">
										<label>Nilai 1</label>
										<input class="form-control" name="nilai_1" value="<?=$data_peserta->nilai_1?>" >
									</div>
									
									<div class="form-group">
										<label>Nilai 2</label>
										<input class="form-control" name="nilai_2" value="<?=$data_peserta->nilai_2?>">
									</div>
									
									<div class="form-group">
										<label>Nilai 3</label>
										<input class="form-control" name="nilai_3" value="<?=$data_peserta->nilai_3?>">
									</div>
									<div class="form-group">
										<label>Nilai Akhir</label>
										<input class="form-control" name="nilai_akhir" value="<?=$data_peserta->nilai_akhir?>">
									</div>
									
						</div>
								
						
						<div class="col-md-6">
									
									<div class="form-group">
										<label>Catatan Nilai 1</label>
										<textarea class="form-control" rows="3" name="catatan_1" value="<?=$data_peserta->catatan_1?>"> <?=$data_peserta->catatan_1?> </textarea>
									</div>
									<div class="form-group">
										<label>Catatan Nilai 2</label>
										<textarea class="form-control" rows="3" name="catatan_2" value="<?=$data_peserta->catatan_2?>"> <?=$data_peserta->catatan_2?> </textarea>
									</div><div class="form-group">
										<label>Catatan Nilai 3</label>
										<textarea class="form-control" rows="3" name="catatan_3" value="<?=$data_peserta->catatan_3?>"> <?=$data_peserta->catatan_3?> </textarea>
									</div>
									<div class ="col-md-6">
									<div class="form-group">
										<label>Lolos / Gagal</label>
										<select class="form-control" name="hasil_audisi" value="<?=$data_peserta->hasil_audisi?>">
											<option value="<?=$data_peserta->hasil_audisi?>"><?=$data_peserta->hasil_audisi?> (Selected) </option>
											<option value="Lolos">LOLOS</option>
											<option value="Gagal">GAGAL</option>
											<option value="Cadangan">CADANGAN</option>
										</select>
									</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
										<label>Jenis Suara</label>
										<select class="form-control" name="jenis_suara" value="<?=$data_peserta->jenis_suara?>">
											<option value="<?=$data_peserta->jenis_suara?>"><?=$data_peserta->jenis_suara?> (Selected) </option>
											<option value="Sopran 1">SOPRAN 1</option>
											<option value="Sopran 2">SOPRAN 2</option>
											<option value="Alto 1">ALTO 1</option>
											<option value="Alto 2">ALTO 2</option>
											<option value="Tenor 1">TENOR 1</option>
											<option value="Tenor 1">TENOR 2</option>
											<option value="Bass 1 ">BASS 1</option>
											<option value="Bass 2">BASS 2</option>
										</select>
									</div>
									</div>
									<input type="submit" name="simpan_nilai_audisi" id="submit"  class="btn btn-primary" value ="Simpan NilaiAudisi"></fieldset>
									
									
								</div>
							</form>
						</div>
					</div>
			</div>
	





		<!--/. Fit N Proper-->
				<div class="panel panel-default">
					<div class="panel-heading">
						Data Audisi
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
					<form class="form-horizontal" role="form" action="" method="POST">
					<fieldset>
						<div class="col-md-12">
									<div class="form-group" >
										<label>1. Motivasi</label>
										<p class="form-control"><?=$data_peserta->motivasi?></p>
									</div>
									<div class="form-group">
										<label>2. Pengalaman Organisasi</label>
										<p class="form-control"><?=$data_peserta->p_organisasi?></p>
									</div>
									<div class="form-group">
										<label>3. Pengalaman di bidang seni</label>
										<p class="form-control"><?=$data_peserta->p_seni?></p>									
									</div>
									<div class="form-group">
										<label>4. Posisi di Kepengurusan VP</label>
										<p class="form-control"><?=$data_peserta->divisi?></p>
									</div>
									<div class="form-group">
										<label>5. Gambaran Paduan Suara yang Baik</label>
										<p class="form-control"><?=$data_peserta->psm_baik?></p>
									</div>
									<div class="form-group">
										<label>6. Tentang Kamu</label>
										<p class="form-control"><?=$data_peserta->cerita_diri?></p>
									</div>
									<div class="form-group">
										<label>7. Dukungan keluarga & orang terdekat</label>
										<p class="form-control"><?=$data_peserta->dukung_ot?></p>
									</div>
									<div class="form-group">
										<label>8. Jenis Penyakit</label>
										<p class="form-control"><?=$data_peserta->penyakit?></p>
									</div>

								</div>
							
						</div>
				
						</fieldset>
					</form>
					
				</div>

				
			
		<!-- /.panel-->
		
		</div><!-- /.col-->
		
	</div>
				
		
		
	</div>	<!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
<?php if(isset($_POST['simpan_nilai_audisi'])){
			include ('config.php');
			$noujian = $_POST['noujian'];
			$nilai_1 = $_POST['nilai_1'];
			$catatan_1= $_POST['catatan_1'];
			$nilai_2 = $_POST['nilai_2'];
			$catatan_2 = $_POST['catatan_2'];			
			$nilai_3 = $_POST['nilai_3'];
			$catatan_3= $_POST['catatan_3'];
			$nilai_akhir = $_POST['nilai_akhir'];
			$hasil_audisi = $_POST['hasil_audisi'];
			$jenis_suara=$_POST['jenis_suara'];


			$q_update_hsl_audisi= "UPDATE `tbl_audisi` SET 
								`nilai_1`='$nilai_1',
								`catatan_1`='$catatan_1',
								`nilai_2`='$nilai_2',
								`catatan_2`='$catatan_2',
								`nilai_3`='$nilai_3',
								`catatan_3`='$catatan_3',
								`nilai_akhir`='$nilai_akhir',
								`hasil_audisi`='$hasil_audisi',
								`jenis_suara`='$jenis_suara'
								 WHERE `noujian`= '$noujian' ";

			$q_update_hslaudisi = mysqli_query($konek, $q_update_hsl_audisi);
							if($q_update_hslaudisi){
								echo "<script>alert('Data Nilai Audisi Peserta $noujian tersimpan')</script>";
								echo "<script>window.location='session_admin_cari.php'</script>";  //biar lgsg kesimpen
								
							}else{
								echo "<script>alert('Data tidak tersimpan')</script>";
								echo "<script>window.location='session_admin_cari.php'</script>"; 
				
							}
			} 
	?>
		
</body>
</html>
