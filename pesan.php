<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_admin_cari.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda</a>";
						}if($status_login=="peserta"){
							echo "<a href='session_peserta.php?'>Kembali ke beranda</a>";
						}elseif($status_login=="admin"){
							echo "<a href='session_admin_cari.php?'>Kembali ke beranda</a>";
						}		
					?>
				</li>
	
			<li class="active"><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Pesan</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Pesan</h1>
			</div>
		</div><!--/.row-->
				
		<div class="panel panel-default">
					<div class="panel-heading">
						Hubungi Kami
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								<!-- Name input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="name">Username</label>
									<div class="col-md-9">
										<input id="name" name="username" type="text"class="form-control" value="<?=$_SESSION['username']?>">
									</div>
								</div>
							

								<!-- Email input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="email">Id Line</label>
									<div class="col-md-9">
										<input name="idline" type="text" placeholder="id Line" class="form-control">
									</div>
								</div>
								
								<!-- Message body -->
								<div class="form-group">
									<label class="col-md-3 control-label" for="message">Pesan Kamu</label>
									<div class="col-md-9">
										<input  class="form-control" id="message" name="pesan" placeholder="Tanya kepada kami, apa yang masih diragukan?" rows="5">
									</div>
								</div>
								
								<!-- Form actions -->
								<div class="form-group">
									<div class="col-md-12 widget-right">
										<button type="submit" class="btn btn-default btn-md pull-right" name="kirim">Kirim Pesan</button>
									</div>
								</div>
							</fieldset>
						</form>
						
					</div>
					
		</div>		

	</div><!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<?php
	
						if(isset($_POST['kirim'])){

    						include('config.php');
							//$konek = new mysqli('localhost','root','root','vocalist_db');
							$username =$_POST['username'];
							$idLine   =$_POST['idline'];
							$pesan    =$_POST['pesan'];
							$q = mysqli_query($konek,"INSERT INTO `tbl_hubungi`(`username`, `pesan`, `IdLine`) VALUES ('$username', '$pesan', '$idLine')")or die(mysqli_eror($konek));

							
							if($q){
								echo "<script>alert('Data tersimpan')</script>";
								echo "<script>window.location='pesan.php'</script>";  //biar lgsg kesimpen
				
							}else{
								echo "<script>alert('Data tidak tersimpan')</script>";
				
							}
							
						}
					?>
</body>
</html>
