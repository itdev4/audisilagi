<!--/.<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if ($_SESSION['status_login']== "peserta"){
		header("location:index.php?pesan=salah");
	}
?> -->
<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>


	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_admin_cari.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">

			<li><a  class="active" href="session_admin_cari.php?"><em class="fa fa-dashboard">&nbsp;</em> Penilaian Audisi </a></li>
			<li><a href="session_admin_addPeserta.php?"><em class="fa fa-calendar">&nbsp;</em> Tambah Peserta </a></li>
			<li><a href="session_admin_peserta.php?"><em class="fa fa-toggle-off">&nbsp;</em> Data Peserta</a></li>
			<li><a href="session_admin_audisi.php?"><em class="fa fa-calendar">&nbsp;</em> Data Audisi</a></li>
			<!--/.<li><a href="import_data.php?"><em class="fa fa-calendar">&nbsp;</em> Import Data</a></li> -->
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Cari  Peserta</li>
			</ol>
		</div><!--/.row-->
	</div>
		
	<?php
			if (isset($_GET['pesan'])){
				if($_GET['pesan']=="gagal"){
				echo "Belum ada peserta yang di cari";
				}elseif($_GET['pesan']=="logout"){
				echo "Anda Telah Logout";
				}else if($_GET['pesan']=="belum_login"){
					echo "Belum ada peserta yang dicari";
				}


			}
	?>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">

			<div class="login-panel panel panel-default">
				<div class="panel-heading">Cari Peserta</div>

				<div class="panel-body">
					<form role="form" method="POST" action="session_admin_input_audisi.php?" >
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="NIM Peserta" name="cari_p" type="text" autofocus="">
							</div>
							
							
							<input type="submit" name="cari_peserta" id="submit"  class="btn btn-primary" value ="Cari"></fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
		
		
	</div><!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	

</body>
</html>
