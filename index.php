<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<?php
		if (isset($_GET['pesan'])){
			if($_GET['pesan']=="gagal"){
			 echo "Login gagal! username dan password salah!";
			}elseif($_GET['pesan']=="logout"){
			echo "Anda Telah Logout";
			}else if($_GET['pesan']=="belum_login"){
				echo "Anda harus Login untuk akses halaman ini";
			}else if($_GET['pesan']=="salah"){
				echo "Anda tidak punya hak akses halaman ini";
			}


		}
	?>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_peserta.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">SELAMAT DATANG !!! Silahkan Login</div>
				<div class="panel-body">
					<form role="form" method="POST" action="cek_login.php?" >
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username (NIM)" name="username" type="text" autofocus="">
								<p class="help-block">contoh : 123190095 </p>
										
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password (Tanggal Lahir)" name="password" type="password" >
								<p class="help-block">----- DDMMYYYY ----</p>
								<p class="help-block">contoh 1 May 2002: 010520002 </p>
							</div>
							<input type="submit" name="submit" id="submit"  class="btn btn-primary" value ="LOGIN"></fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	
</body>
</html>
