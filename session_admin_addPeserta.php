<! <?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if ($_SESSION['status_login']== "peserta"){
		header("location:index.php?pesan=salah");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_admin_cari.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><a href="session_admin_cari.php?"><em class="fa fa-dashboard">&nbsp;</em> Penilaian Audisi </a></li>
			<li><a  class="active" href="session_admin_addPeserta.php?"><em class="fa fa-calendar">&nbsp;</em> Tambah Peserta </a></li>
			<li><a href="session_admin_peserta.php?"><em class="fa fa-toggle-off">&nbsp;</em> Data Peserta</a></li>
			<li><a href="session_admin_audisi.php?"><em class="fa fa-calendar">&nbsp;</em> Data Audisi</a></li>
			<!--/.<li><a href="import_data.php?"><em class="fa fa-calendar">&nbsp;</em> Import Data</a></li> -->
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Tambah Peserta</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Tambah Peserta</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-container">
		<div class="panel-body">
					<div class="col-md-6">
							<form role="form"  method="post">
									<div class="form-group">
										<label>Nama Lengkap</label>
										<input class="form-control" name="name"  >
									</div>
									
									<div class="form-group">
										<label>NIM</label>
										<input class="form-control" name="noujian" >
									</div>
									
					
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input class="form-control" placeholder="DDMMYYY" name="tgllhr" >
									</div>
									<div class="form-group">
										<label>ID Line</label>
										<input class="form-control" name="id_line" >
									</div>
									<div class="form-group">
										<label>No Telpon Pribadi</label>
										<input class="form-control" name="no_hp" >
									</div>
									
								
								
								
						</div>
								<!--/. Ganti Kolom -->
						
						<div class="col-md-6">
									
									<div class="form-group">
										<label>Fakultas</label>
										<select class="form-control" name="fakultas">
											<option disabled='disabled' >-Pilih Fakultas-</option>
											<option value="FEB">F. Ekonomi dan Bisnis</option>
											<option value="FISIP">F. Ilmu Sosial & Ilmu Politik</option>
											<option value="FAPERTA">F. Pertanian</option>
											<option value="FTI">F. Teknik Industri</option>
											<option value="FTM">F. Teknologi Mineral</option>
										</select>
									</div>
									<div class="form-group">
										<label>Program Studi</label>
										<select  class="form-control" name="prodi" ">
											<option disabled='disabled' '>-Pilih Program Studi-</option>
											<option value="Agribisnis">Agribisnis</option>
											<option value="Agroteknologi">Agroteknologi</option>
											<option value="Akuntansi">Akuntansi</option>
											<option value="Teknik Kimia">Teknik Kimia</option>
											<option value="Ekonomi Pembangunan">Ekonomi Pembangunan</option>
											<option value="Ilmu Administrasi Bisnis"> Ilmu Administrasi Bisnis</option>
											<option value="Ilmu Hubungan Internasional">Ilmu Hubungan Internasional</option>
											<option value="Ilmu Komunikasi">Ilmu Komunikasi</option>
											<option value="Manajemen">Manajemen</option>
											<option value="Teknik Geofisika">Teknik Geofisika</option>
											<option value="Teknik Geologi">Teknik Geologi</option>
											<option value="Teknik Industri">Teknik Industri</option>
											<option value="Teknik Informatika">Teknik Informatika</option>
											<option value="Teknik Kimia S1">Teknik Kimia S1</option>
											<option value="Teknik Kimia D3">Teknik Kimia D3</option>
											<option value="Teknik Lingkungan">Teknik Lingkungan</option>
											<option value="Teknik Perminyakan">Teknik Perminyakan</option>
											<option value="Teknik Pertambangan">Teknik Pertambangan</option>
											<option value="Teknik Metalurgi">Teknik Metalurgi</option>
											<option value="Ilmu Tanah">Ilmu Tanah</option>
											<option value="Sistem Informasi">Sistem Informasi</option>
											<option value="Hubungan Masyarakat">Hubunga Masyarakat</option>
									
										</select>
									</div>
									<div class="form-group">
										<label>Status Bayar</label>
										<select class="form-control" name="bayar">
											<option disabled='disabled' >-Status Bayar-</option>
											<option value="Lunas">LUNAS</option>
											<option value="Tunda">TUNDA</option>
										</select>
									</div>
									
									<button type="submit" name="simpan_addPeserta" class="btn btn-primary">Tambahkan Peserta</button>
									
								</div>
							</form>
						</div>
					</div>
		</div>
		
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
	};
	</script>

	<?php if(isset($_POST['simpan_addPeserta'])){
			include ('config.php');
			$noujian = $_POST['noujian'];
			$name = $_POST['name'];
			$tgllhr = $_POST['tgllhr'];
			$id_line = $_POST['id_line'];
			$no_hp = $_POST['no_hp'];
			$fakultas = $_POST['fakultas'];
			$prodi = $_POST['prodi'];
			$bayar=$_POST['bayar'];

			$qtblSiswa = "INSERT INTO `tbl_siswa`
			(`noujian`, `name`, `tgllhr`,  `prodi`, `no_hp`, `id_line`,  `fakultas`, `bayar`) 
			VALUES ('$noujian','$name', '$tgllhr', '$prodi', '$no_hp', '$id_line', '$fakultas', '$bayar')";
			$qtblUser = "INSERT INTO `tbl_user`(`username`, `pass`, `status`, `nama`) 
			VALUES ('$noujian','$tgllhr','peserta','$name')";
			$qtblAudisi = "INSERT INTO `tbl_audisi`(`noujian`) 
			VALUES ('$noujian')";
						
			$q_tblAudisi = mysqli_query($konek, $qtblAudisi);
			$q_tblSiswa = mysqli_query($konek, $qtblSiswa);
			$q_tblUser = mysqli_query($konek, $qtblUser);
							if($q_tblSiswa && $q_tblUser){
								echo "<script>alert('Data tersimpan')</script>";
								echo "<script>window.location='session_admin_addPeserta.php'</script>";  //biar lgsg kesimpen
				
							}else{
								echo "<script>alert('Data tidak tersimpan')</script>";
				
							}
			} 
	?>		
</body>
</html>