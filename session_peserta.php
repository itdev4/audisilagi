<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if ($_SESSION['status_login']=="admin"){
		header("location:index.php?pesan=salah");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php
		include('config.php');
		$username = $_SESSION['username'];
		$q = mysqli_query($konek,"SELECT * FROM `tbl_siswa` WHERE noujian=$username");
    	$data_peserta= mysqli_fetch_object($q);
	?>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_peserta.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?=$data_peserta->name?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class="active"><a href="session_peserta.php?"><em class="fa fa-dashboard">&nbsp;</em> Beranda Peserta</a></li>
			<li><a href="session_peserta_formulir.php?"><em class="fa fa-calendar">&nbsp;</em> Formulir</a></li>
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Beranda Peserta 2020</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Selamat Datang Peserta Audisi 2020</h1>
			</div>
		</div><!--/.row-->
		<div class="panel panel-default">
					<div class="panel-heading">
						<?=$data_peserta->name?>
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								<div class="form-group">
									<label class="col-md-3 control-label" for="name">Nama</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->name?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tgllhr">Tanggal Lahir </label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->tgllhr?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="jurusan">Program Studi</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->prodi?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="name">Waktu Audisi</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" >- Comming Soon -</label>
									</div>
								</div>
							</fieldset>
						</form>
					</div><!--/.col-->
		</div><!--/.row-->


		<!--/. AGENDAAAAAA-->
		<div class="panel panel-default">
					<div class="panel-heading">
						Agenda Audisi 2020
						
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
						<div class="panel panel-default ">
					
					<div class="panel-body timeline-container">
						<ul class="timeline">
							<li>
								<div class="timeline-badge"><i class="glyphicon glyphicon-pushpin"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Pendaftaran</h4>
									</div>
									<div class="timeline-body">
										<p>25 September 2020</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge primary"><i class="glyphicon glyphicon-link"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Pra Audisi 2020</h4>
									</div>
									<div class="timeline-body">
										<p>01 Oktober 2020</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge"><i class="glyphicon glyphicon-camera"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Mentoring</h4>
									</div>
									<div class="timeline-body">
										<p>02 - 07 Oktober 2020</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge"><i class="glyphicon glyphicon-paperclip"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">!!! Audisi !!!</h4>
									</div>
									<div class="timeline-body">
										<p>13 - 18 Oktober 2020</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge"><i class="glyphicon glyphicon-paperclip"></i></div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Pengumuman</h4>
									</div>
									<div class="timeline-body">
										<p>23 Oktober 2020</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
		</div><!--/.row-->

	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		
</body>
</html>