<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}
?>
<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<!--/.	<?php
		include('config.php');
		$username = '123170095';
		$q = mysqli_query($konek,"SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian WHERE tbl_siswa.noujian='$username'");
    	$data_peserta= mysqli_fetch_object($q);
	?>
-->

	<?php
	session_start();
		include('config.php');
	
		$cari_p= $_POST['cari_p'];
		$q = mysqli_query($konek, "SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian WHERE tbl_siswa.noujian='$username'")or die(mysqli_eror($konek));
		$data_peserta= mysqli_fetch_object($q);

	?>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_peserta.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?echo $_SESSION['username'];?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><a  class="active" href="session_admin_cari.php?"><em class="fa fa-dashboard">&nbsp;</em> Penilaian Audisi </a></li>
			<li><a href="session_admin_addPeserta.php?"><em class="fa fa-calendar">&nbsp;</em> Tambah Peserta </a></li>
			<li><a href="session_admin_peserta.php?"><em class="fa fa-toggle-off">&nbsp;</em> Data Peserta</a></li>
			<li><a href="session_admin_audisi.php?"><em class="fa fa-calendar">&nbsp;</em> Data Audisi</a></li>
			<!--/.<li><a href="import_data.php?"><em class="fa fa-calendar">&nbsp;</em> Import Data</a></li> -->
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
	
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</li>
				<li class="active">Penilaian Audisi</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Penilaian Audisi</h1>
			</div>
		</div><!--/.row-->
		

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Penilaian Audisi
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
					</div>
					<div class="panel-body">
								<form role="form" action="session_admin.php" method="post">

							<div class="col-md-4">
							
							<input class="form-control" type="text" name="cari_noujian" placeholder="Cari NIM Peserta">
			
							</div>
							<div class="col-md-3">
						
								<button type="submit" name="cari_peserta" class="btn btn-primary" >Cari Peserta</button>
				
							</div>
								
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								<div class="form-group">
									<label class="col-md-2 control-label" for="name">Nama</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->name?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="tgllhr">Tanggal Lahir </label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->tgllhr?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="jurusan">Program Studi</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" ><?=$data_peserta->prodi?> </label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="name">Waktu Audisi</label>
									<div class="col-md-9">
										<label  type="text"  class=" form-control" > 13 Oktober 2020 (10.30 - 11.00) </label>
									</div>
									
									<button type="submit" class="btn btn-primary" name="simpan_data_audisi">Submit Button</button>
									<button type="reset" class="btn btn-default">Reset Button</button>
								</div>
							</fieldset>
						</form>
					</div><!--/.col-->



					



				</div>
			</div>
		</div><!--/.row-->
		
		
		
	</div>	<!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

	<?php if(isset($_GET['cari_peserta'])){
			include ('config.php');
			$cari_noujian = $_POST['cari_noujian'];


			$q_cari = mysqli_query($konek,"SELECT * FROM `tbl_siswa` WHERE `noujian`='$cari_noujian'")or die(mysqli_eror($konek));
			$data_peserta= mysqli_fetch_object($q_cari);

							
							if($q_update){
								echo "<script>alert('Data Peserta ditemukan!')</script>";
								echo "<script>window.location='session_admin.php'</script>";  //biar lgsg kesimpen
				
							}else{
								echo "<script>alert('Data tidak ditemukan')</script>";
				
							}
			} 
	?>
		
</body>
</html>
