<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if ($_SESSION['status_login'!="superadmin"]){
		header("location:index.php?pesan=salah");
	}
?>
<?php
    include('config.php');

    $q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

    $status_login= mysqli_fetch_object($q)->status;

        $_SESSION ['status_login']=$status_login;
        if($status_login=="superadmin"){
            
        }
	
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<?php
			include('config.php');
				$username=$_SESSION['username'];
			$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

			$status_login= mysqli_fetch_object($q)->status;

				$_SESSION ['status_login']=$status_login;
				if($status_login=="superadmin"){
					echo "Kembali ke beranda superadmin";
				}
			
?>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_admin_cari.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><a href="session_super.php?"><em class="fa fa-dashboard">&nbsp;</em> Beranda Superadmin </a></li>
			<li><a href="session_admin_cari.php?"><em class="fa fa-dashboard">&nbsp;</em> Beranda Admin </a></li>
			<li><a href="session_peserta.php?"><em class="fa fa-dashboard">&nbsp;</em> Beranda Peserta</a></li>
			<li><a href="session_admin_addPeserta.php?"><em class="fa fa-calendar">&nbsp;</em> Tambah Peserta </a></li>
			<li><a href="session_admin_peserta.php?"><em class="fa fa-toggle-off">&nbsp;</em> Data Peserta</a></li>
			<li><a href="session_admin_audisi.php?"><em class="fa fa-calendar">&nbsp;</em> Data Nilai</a></li>
			<!--<li><a href="import_data.php?"><em class="fa fa-calendar">&nbsp;</em> Import Data</a></li>-->
			<li><a href="session_peserta_formulir.php?"><em class="fa fa-calendar">&nbsp;</em> Formulir</a></li>
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">BERANDA SUPER ADMIN 2020</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">BERANDA SUPER ADMIN 2020</h1>
			</div>
		</div><!--/.row-->
		
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		
</body>
</html>