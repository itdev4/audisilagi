-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 19, 2020 at 11:24 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `vocalistdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audisi`
--

CREATE TABLE `tbl_audisi` (
  `noujian` int(10) NOT NULL,
  `waktu_audisi` int(11) DEFAULT NULL,
  `motivasi` varchar(200) DEFAULT NULL,
  `p_organisasi` varchar(200) DEFAULT NULL,
  `p_seni` varchar(200) DEFAULT NULL,
  `divisi` varchar(200) DEFAULT NULL,
  `psm_baik` varchar(200) DEFAULT NULL,
  `cerita_diri` varchar(200) DEFAULT NULL,
  `dukung_ot` varchar(200) DEFAULT NULL,
  `penyakit` varchar(200) DEFAULT NULL,
  `nilai_1` varchar(11) DEFAULT NULL,
  `catatan_1` varchar(200) DEFAULT NULL,
  `nilai_2` varchar(11) DEFAULT NULL,
  `catatan_2` varchar(200) DEFAULT NULL,
  `nilai_3` varchar(11) DEFAULT NULL,
  `catatan_3` varchar(200) DEFAULT NULL,
  `nilai_akhir` varchar(20) DEFAULT NULL,
  `hasil_audisi` varchar(10) DEFAULT NULL,
  `jenis_suara` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_audisi`
--

INSERT INTO `tbl_audisi` (`noujian`, `waktu_audisi`, `motivasi`, `p_organisasi`, `p_seni`, `divisi`, `psm_baik`, `cerita_diri`, `dukung_ot`, `penyakit`, `nilai_1`, `catatan_1`, `nilai_2`, `catatan_2`, `nilai_3`, `catatan_3`, `nilai_akhir`, `hasil_audisi`, `jenis_suara`) VALUES
(0, NULL, 'Motivasi bawah', 'Organisasi 1', 'Pengalaman Seni 1', 'kepengurusan 1', 'Paduan Suara satu', 'Tentang satu', 'Keluarga satu', 'Penyakit satu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, NULL, 'Motivasi yang motivasi', 'Organisasi Baru', 'Seni baru', 'divisi baru', 'psm baru ', 'tentang aku', 'dukungan keluarga', 'penyakit', 'nilai 1', '  Catatan 1  ', 'nilai 2', '  Catatan Nilai   ', 'nilai 3', '  Catatan 3  ', '', 'Gagal', 'Alto 1'),
(222, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Gagal', 'Tenor 1'),
(116170022, NULL, 'Motivasi Rei', 'Organisasi rei', 'Seni Rei', 'Latihan Rei', 'PSM Rei', 'Ini Rei ', 'Dukungan keluarga rei', 'Penyakit rei', NULL, NULL, NULL, NULL, NULL, NULL, '', 'Lolos', 'Tenor 1'),
(123170095, 1, 'Motivasi', 'organisaasi ed', 'Seni ed', 'Kepengurusan', 'PSM BAIK', 'Cerita rei', 'Dukung keluarga Ed', 'penyakittt', NULL, NULL, NULL, NULL, NULL, NULL, '', 'Cadangan', 'Sopran 1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hubungi`
--

CREATE TABLE `tbl_hubungi` (
  `id_hubungi` int(10) NOT NULL,
  `username` varchar(150) NOT NULL,
  `pesan` text NOT NULL,
  `IdLine` varchar(30) NOT NULL,
  `Jawaban` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_hubungi`
--

INSERT INTO `tbl_hubungi` (`id_hubungi`, `username`, `pesan`, `IdLine`, `Jawaban`) VALUES
(18, 'Dandi Priambodo', 'Kak, permisi. Mau minta partitur meplalian', '', ''),
(20, 'Rizki dian pamungkas', 'Kenapa saya tidak bisa melihat hasil audisi saya, padahal sudah saya coba berkali2, ', '', ''),
(21, '124190042', 'ujicoba', 'llala', NULL),
(22, '124190042', 'nanana', '', NULL),
(23, '124190042', 'lagiii', '', NULL),
(24, '124190042', 'coba id line tadi eror', 'adeok', NULL),
(25, '124190042', 'HAHAHHAHA', 'jjj', NULL),
(26, '124190042', 'llll', 'kkk', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `noujian` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `tgllhr` varchar(8) NOT NULL,
  `tempat_lhr` varchar(200) DEFAULT NULL,
  `alamat_jogja` varchar(200) DEFAULT NULL,
  `alamat_asal` varchar(200) DEFAULT NULL,
  `prodi` varchar(200) DEFAULT NULL,
  `no_asal` varchar(200) DEFAULT NULL,
  `no_jogja` varchar(200) DEFAULT NULL,
  `no_hp` varchar(200) DEFAULT NULL,
  `id_line` varchar(50) DEFAULT NULL,
  `gol_darah` varchar(2) DEFAULT NULL,
  `fakultas` varchar(30) DEFAULT NULL,
  `bayar` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`noujian`, `name`, `tgllhr`, `tempat_lhr`, `alamat_jogja`, `alamat_asal`, `prodi`, `no_asal`, `no_jogja`, `no_hp`, `id_line`, `gol_darah`, `fakultas`, `bayar`) VALUES
('000', 'NolNolNol Nol Nol', '000', 'Tempat Lahir Nol', NULL, NULL, 'Teknik Industri', NULL, NULL, '00000000000000', 'Nol0', NULL, 'FTM', ''),
('111', 'Satu SatuSatu', '111', 'Tempat Lahir Satu', '      Alamat Ganti      ', 'Alamat Asal 1', 'Agroteknologi', '081902786823', '01783t', '11188881111', 'Satu', NULL, 'FAPERTA', '1'),
('116170022', 'Reinhart Samuel', '18111998', 'Jakarta', 'Depan indomaret', 'Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei Bekasi Rei ', 'Hubungan Masyarakat', '019973526728', '0885634345', '08121212121212121212', 'rei', NULL, 'FTM', 'Lunas'),
('123170095', 'Edwina Ayu Christy', '010599', 'Jakarta', '  Alamat 1', 'Alamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat AsalAlamat Asal', 'Agribisnis', '0214412001', '0885634345', '08124831470', 'Eddie', 'AB', 'FEB', 'Tunda'),
('123456782', 'Peserta baru 2', '09092020', 'Yogyakarta', NULL, NULL, 'Teknik Informatika', NULL, NULL, '09092020', 'pesertaBaru2', NULL, 'FTI', ''),
('123456783', 'Peserta baru 3', '09092020', 'Yogyakarta3', NULL, NULL, 'Teknik Informatika', NULL, NULL, '09092020', 'pesertaBaru3', NULL, 'FTM', ''),
('123456789', 'name', 'tgllhr', 'tempat_lhr', NULL, NULL, 'prodi', NULL, NULL, 'no_hp', 'id_line', NULL, 'fakultas', 'bayar'),
('222', 'dua dua dua', '222', '222', NULL, NULL, 'Sistem Informasi', NULL, NULL, '222', '222', NULL, 'FTI', 'Tunda');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `pass`, `status`, `nama`) VALUES
(12, '123170095', '010599', 'peserta', 'Peserta Edwina'),
(17, 'ella', 'ella', 'admin', 'Ella'),
(18, 'aji', 'aji', 'admin', 'Aji'),
(19, 'umbul', 'umbul', 'admin', 'Khasbulloh'),
(21, 'ed', 'ed', 'superadmin', 'ed'),
(22, '123456783', '09092020', 'peserta', 'Peserta baru 3'),
(23, '116170022', '18111998', 'peserta', 'Reinhart Samuel'),
(30, '111', '111', 'peserta', 'Satu SatuSatu'),
(32, '222', '222', 'peserta', 'dua dua dua'),
(34, 'edd', 'edd', 'admin', 'edd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_audisi`
--
ALTER TABLE `tbl_audisi`
  ADD PRIMARY KEY (`noujian`);

--
-- Indexes for table `tbl_hubungi`
--
ALTER TABLE `tbl_hubungi`
  ADD PRIMARY KEY (`id_hubungi`);

--
-- Indexes for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`noujian`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_hubungi`
--
ALTER TABLE `tbl_hubungi`
  MODIFY `id_hubungi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
