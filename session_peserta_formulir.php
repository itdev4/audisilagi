<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if ($_SESSION['status_login']== "admin" ){
		header("location:index.php?pesan=salah");
	}
?>
<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<?php
		include('config.php');
		$username = $_SESSION['username'];
		$q = mysqli_query($konek,"SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian WHERE tbl_siswa.noujian='$username'");
    	$data_peserta= mysqli_fetch_object($q);
	?>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_peserta.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?=$data_peserta->name?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><a href="session_peserta.php?"><em class="fa fa-dashboard">&nbsp;</em> Beranda Peserta</a></li>
			<li class="active"><a href="session_peserta_formulir.php?"><em class="fa fa-calendar">&nbsp;</em> Formulir</a></li>
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</li>
				<li class="active">Formulir Pendaftaran</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Formulir Pendaftaran </h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				
				<div class="panel panel-default">
					<div class="panel-heading">Data Diri
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
				</div>
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" action="" method="post">
									<div class="form-group">
										<label>Nama Lengkap</label>
										<input class="form-control" name="name" value="<?=$data_peserta->name?>" >
									</div>
									
									<div class="form-group">
										<label>NIM</label>
										<input class="form-control" name="noujian" value="<?=$data_peserta->noujian?>">
									</div>
									
									<div class="form-group">
										<label>Tempat Lahir</label>
										
										<input class="form-control" name="tempat_lhr" placeholder="Tempat Lahir" value="<?=$data_peserta->tempat_lhr?>">
									</div>
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<p class="help-block">DDMMYYY</p>
										<input class="form-control" name="tgllhr" value="<?=$data_peserta->tgllhr?>">
									</div>
									<div class="form-group">
										<label>ID Line</label>
										<input class="form-control" name="id_line" value="<?=$data_peserta->id_line?>">
									</div>
									<div class="form-group">
										<label>No Telpon Pribadi</label>
										<input class="form-control" name="no_hp" value="<?=$data_peserta->no_hp?>">
									</div>
									<div class="form-group">
										<label>Alamat Asal</label>
										<textarea  class="form-control" rows="3" name="alamat_asal" value="<?=$data_peserta->alamat_asal?>"><?=$data_peserta->alamat_asal?></textarea>
									</div>
									<div class="form-group">
										<label>No. Telpon Asal</label>
										<input class="form-control" name="no_asal" value="<?=$data_peserta->no_asal?>">
									</div>
								
								
								
						</div>
								<!--/. Ganti Kolom -->
						
						<div class="col-md-6">
									<div class="form-group">
										<label>Alamat Yogyakarta</label>
										<textarea class="form-control" rows="3" name="alamat_jogja" value="<?=$data_peserta->alamat_jogja?>"> <?=$data_peserta->alamat_jogja?> </textarea>
									</div>
									<div class="form-group">
										<label>No. Telpon Yogyakarta</label>
										<input class="form-control" name="no_jogja" value="<?=$data_peserta->no_jogja?>">
									</div>
									<div class="form-group">
										<label>Fakultas</label>
										<select class="form-control" name="fakultas" value="<?=$data_peserta->fakultas?>">
											
											<option value="<?=$data_peserta->fakultas?>"><?=$data_peserta->fakultas?> (Selected) </option>
											<option value="FEB">FEB</option>
											<option value="FISIP">FISIP</option>
											<option value="FAPERTA">FAPERTA</option>
											<option value="FTI">FTI</option>
											<option value="FTM">FTM</option>
										</select>
									</div>
									<div class="form-group">
										<label>Program Studi</label>
										<select  class="form-control" name="prodi">
											<option value="<?=$data_peserta->prodi?>"><?=$data_peserta->prodi?> (Selected) </option>
											<option value="Agribisnis">Agribisnis</option>
											<option value="Agroteknologi">Agroteknologi</option>
											<option value="Akuntansi">Akuntansi</option>
											<option value="Teknik Kimia">Teknik Kimia</option>
											<option value="Ekonomi Pembangunan">Ekonomi Pembangunan</option>
											<option value="Ilmu Administrasi Bisnis"> Ilmu Administrasi Bisnis</option>
											<option value="Ilmu Hubungan Internasional">Ilmu Hubungan Internasional</option>
											<option value="Ilmu Komunikasi">Ilmu Komunikasi</option>
											<option value="Manajemen">Manajemen</option>
											<option value="Teknik Geofisika">Teknik Geofisika</option>
											<option value="Teknik Geologi">Teknik Geologi</option>
											<option value="Teknik Industri">Teknik Industri</option>
											<option value="Teknik Informatika">Teknik Informatika</option>
											<option value="Teknik Kimia S1">Teknik Kimia S1</option>
											<option value="Teknik Kimia D3">Teknik Kimia D3</option>
											<option value="Teknik Lingkungan">Teknik Lingkungan</option>
											<option value="Teknik Perminyakan">Teknik Perminyakan</option>
											<option value="Teknik Pertambangan">Teknik Pertambangan</option>
											<option value="Teknik Metalurgi">Teknik Metalurgi</option>
											<option value="Ilmu Tanah">Ilmu Tanah</option>
											<option value="Sistem Informasi">Sistem Informasi</option>
											<option value="Hubungan Masyarakat">Hubunga Masyarakat</option>
									
										</select>
									</div>

								<!--/.
									<div class="form-group">
										<label>Golongan Darah</label>
										<select class="form-control">
											<option value="<?=$data_peserta->gol_darah?>"><?=$data_peserta->gol_darah?> (Selected)</option>
											<option value="A">A</option>
											<option value="B">B</option>
											<option value="AB">AB</option>
											<option value="O">O</option>
											
										</select>
									</div>
									-->
									<div class="form-group">
										<label>Status Bayar</label>
										<label  type="text"  class=" form-control"><?=$data_peserta->bayar?></label>
								
									</div>
									<button type="submit" name="simpan_data_pribadi" class="btn btn-primary">Simpan Data Pribadi</button>
									<button type="reset" class="btn btn-default">Reset Button</button>
								
								</div>
								
						

									
						</form>
						
					
				</div><!-- /.panel-->
			</div><!-- /.col-->
		<!--/. FORM AUDISI-->
		<div class="panel panel-default">
					<div class="panel-heading">
						Data Audisi
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
					<form class="form-horizontal" role="form" action="" method="post">
						<div class="col-md-12">
									<!--<div class="form-group" name="waktu_audisi">
										<label>Waktu Audisi</label>
										<p class="help-block">Pilih hari & waktu yang tidak 	mengganggu jadwal kuliah</p>
										
										<div class="col-md-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari A (waktu 1)
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari A (waktu 1)
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari B (waktu 1)
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari B (waktu 1)
												</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari C (waktu 1)
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari C (waktu 1)
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari D (waktu 1)
												</label>
											</div>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="">Hari D (waktu 1)
												</label>
											</div>
										</div>
										
									</div> -->
									<div class="form-group" >
										<label>1. Motivasi</label>
										<textarea class="form-control" name="motivasi" rows="3" placeholder="Kenapa kamu tertarik ikut VP? Motivasinya apa?" value="<?=$data_peserta->motivasi?>"><?=$data_peserta->motivasi?></textarea>
									</div>
									<div class="form-group">
										<label>2. Pengalaman Organisasi</label>
										<p class="help-block">Pengalaman organisasi selama ini, baik bersifat keagamaan, politik, maupun bentuk-bentuk organisasi lainnya dan terlibat sebagai apa Anda dalam organisasi tersebut?</p>
										<textarea class="form-control" name="p_organisasi" rows="3" placeholder="Pengalaman Organisasi" value="<?=$data_peserta->p_organisasi?>"><?=$data_peserta->p_organisasi?></textarea>
									</div>
									<div class="form-group">
										<label>3. Pengalaman di bidang seni</label>
										<p class="help-block">Apakah Anda memiliki pengalaman atau kegiatan seni lainnya terutama dalam musikalitas? Jelaskan!</p>
										<textarea class="form-control"  name ="p_seni" rows="3" placeholder="Pengalaman di bidang seni" value="<?=$data_peserta->p_seni?>"><?=$data_peserta->p_seni?></textarea>
									</div>
									<div class="form-group">
										<label>4. Posisi di Kepengurusan VP</label>
										<p class="help-block">Posisi seperti apa yang tepat diberikan kepada Anda, apabila suatu saat nanti Anda duduk dalam kepengurusan PSM?</p>
										<textarea class="form-control"  name="divisi" rows="3" placeholder="Posisi di kepengurusan VP" value="<?=$data_peserta->divisi?>"><?=$data_peserta->divisi?></textarea>
									</div>
									<div class="form-group">
										<label>5. Gambaran Paduan Suara yang Baik</label>
										<p class="help-block">Berikan gambaran Anda mengenai suatu Paduan Suara dan apa yang diperlukan suatu Paduan                                
   											 Suara agar dapat dinilai “BAIK”</p>
										<textarea class="form-control" name="psm_baik" rows="3" placeholder="Paduan suara yang BAIK" value="<?=$data_peserta->psm_baik?>"><?=$data_peserta->psm_baik?></textarea>
									</div>
									<div class="form-group">
										<label>6. Tentang Kamu</label>
										<p class="help-block">Ceritakan DIRI ANDA! </p>
										<textarea class="form-control" name="cerita_diri" rows="3" placeholder="ceritakan pribadimu, dan dirimu" value="<?=$data_peserta->cerita_diri?>"><?=$data_peserta->cerita_diri?></textarea>
									</div>
									<div class="form-group">
										<label>7. Dukungan keluarga & orang terdekat</label>
										<p class="help-block">Seberapa besar dukungan yang diberikan oleh keluarga terhadap kegiatan di luar perkuliahan demikian pula dengan orang “terdekat” Anda!</p>
										<textarea class="form-control" name="dukung_ot" namerows="3" placeholder="Dukungan keluarga" value="<?=$data_peserta->dukung_ot?>"><?=$data_peserta->dukung_ot?></textarea>
									</div>
									<div class="form-group">
										<label>8. Jenis Penyakit</label>
										<p class="help-block">Apakah ada pernah/memiliki riwayat penyakit ? Jenis penyakit yang pernah diderita (bila ada).</p>
										<textarea class="form-control" name ="penyakit" rows="3" placeholder="Jenis penyakit yang pernah diderita" value="<?=$data_peserta->penyakit?>"><?=$data_peserta->penyakit?></textarea>
									</div>
									
									<button type="submit" class="btn btn-primary" name="simpan_data_audisi">Submit Button</button>
									<button type="reset" class="btn btn-default">Reset Button</button>
								</div>
							
						</div>
					</form>
					</div>


		</div>
		
	</div><!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	




	<?php if(isset($_POST['simpan_data_pribadi'])){
			include ('config.php');
			$noujian = $_SESSION['username'];
			$name = $_POST['name'];
			$tempat_lhr = $_POST['tempat_lhr'];
			$tgllhr = $_POST['tgllhr'];
			$id_line = $_POST['id_line'];
			$no_hp = $_POST['no_hp'];
			$alamat_asal = $_POST['alamat_asal'];
			$alamat_jogja = $_POST['alamat_jogja'];
			$no_asal = $_POST['no_asal'];
			$no_jogja = $_POST['no_jogja'];
			$fakultas = $_POST['fakultas'];
			$prodi = $_POST['prodi'];

			$qupdate_tblsiswa = "UPDATE `tbl_siswa` SET 
			`tgllhr`='$tgllhr',
			`tempat_lhr`='$tempat_lhr',
			`alamat_jogja`='$alamat_jogja',
			`alamat_asal`='$alamat_asal',
			`prodi`='$prodi',
			`no_asal`='$no_asal',
			`no_jogja`='$no_jogja',
			`no_hp`='$no_hp',
			`id_line`='$id_line',
			`fakultas`='$fakultas',
			`bayar`='1' WHERE `noujian`='$noujian'";


			
	
			$q_updatetblsiswa = mysqli_query($konek, $qupdate_tblsiswa);
			


							if($q_updatetblsiswa ){
								echo "<script>alert('Data tersimpan')</script>";
								echo "<script>window.location='session_peserta_formulir.php'</script>";  //biar lgsg kesimpen
				
							}else{
								echo "<script>alert('Data tidak tersimpan')</script>";
				
							}
			} 
	?>
		<?php if(isset($_POST['simpan_data_audisi'])){
			include ('config.php');
			$noujian = $_SESSION['username'];
			$motivasi = $_POST['motivasi'];
			$p_organisasi = $_POST['p_organisasi'];
			$p_seni = $_POST['p_seni'];
			$divisi = $_POST['divisi'];
			$psm_baik = $_POST['psm_baik'];
			$cerita_diri = $_POST['cerita_diri'];
			$dukung_ot = $_POST['dukung_ot'];
			$penyakit = $_POST['penyakit'];
			$waktu_audisi=$_POST['waktu_audisi'];
			
			$qupdate_tblaudisi ="UPDATE `tbl_audisi` SET 
			`motivasi`= '$motivasi',
			`p_organisasi`='$p_organisasi',
			`p_seni`='$p_seni',
			`divisi`= '$divisi',
			`psm_baik`='$psm_baik',
			`cerita_diri`='$cerita_diri',
			`dukung_ot`='$dukung_ot',
			`penyakit`='$penyakit' WHERE `noujian`='$noujian'";
			$q_updatetblaudisi = mysqli_query($konek, $qupdate_tblaudisi);


							if($q_updatetblaudisi){
								echo "<script>alert('Data $data_peserta->noujian tersimpan Audisi ')</script>";
								echo "<script>window.location='session_peserta_formulir.php'</script>";  //biar lgsg kesimpen
				
							}else{
								echo "<script>alert('Data tidak tersimpan')</script>";
				
							}
			} 
	?>


</body>
</html>
