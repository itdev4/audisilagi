
<!--/.<?php
	session_start();
	if(empty($_SESSION['username'])){
		header("location:index.php?pesan=belum_login");
	}else if($_SESSION['status_login']== "peserta"){
		header("location:index.php?pesan=salah");
	}
?> -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AUDISI VOCALISTA PARADISSO 20</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="session_admin_cari.php?"><span>Audisi</span>VP 2020</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo $_SESSION['username'];?></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li><a href="session_admin_cari.php?"><em class="fa fa-dashboard">&nbsp;</em> Penilaian Audisi </a></li>
			<li><a href="session_admin_addPeserta.php?"><em class="fa fa-calendar">&nbsp;</em> Tambah Peserta </a></li>
			<li><a href="session_admin_peserta.php?"><em class="fa fa-toggle-off">&nbsp;</em> Data Peserta</a></li>
			<li><a  class="active" href="session_admin_audisi.php?"><em class="fa fa-calendar">&nbsp;</em> Data Audisi</a></li>
			<!--/.<li><a href="import_data.php?"><em class="fa fa-calendar">&nbsp;</em> Import Data</a></li> -->
			<li><a href="pesan.php?"><em class="fa fa-calendar">&nbsp;</em> Pesan</a></li>
			<li><a href="logout.php?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
	
		
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><?php
					include('config.php');
						$username=$_SESSION['username'];
					$q = mysqli_query($konek, "SELECT * FROM tbl_user WHERE username='$username'")or die(mysqli_eror($konek));

					$status_login= mysqli_fetch_object($q)->status;
						$_SESSION ['status_login']=$status_login;
						if($status_login=="superadmin"){
							echo "<a href='session_super.php?'>Kembali ke beranda Superadmin</a>";
						}	
					?>
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Data Audisi</li>
			</ol>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Data Peserta</h1>
			</div>
		</div><!--/.row-->
			<!--/.Tabel peserta-->
			<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Data Audisi 2020
								
								<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
							</div>
							
							<!--/ Form edit-->
							<div class="panel-body">
								<div class="canvas-wrapper">
								<?php
								include('config.php');
								$q = mysqli_query($konek, "SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian ORDER BY tbl_siswa.noujian ")or die(mysqli_eror($konek));
							?>
								<table class="table table-striped table-hover">
								<thead>
								<th>NIM</th>
								<th>Nama Mahasiswa</th>
								<th>Program Studi</th>
								<th>Lolos/Gagal</th>
								<th>Jenis Suara</th>
								<th>Nilai Akhir</th>
								</tr>
								</thead>
								
							
								<form action="" method="post">
								<?php
								while($data_peserta = mysqli_fetch_array($q))
								{?>	
									<tr>
										<td><label name="noujian" value="<?=$data_peserta["noujian"]?>" ><?=$data_peserta["noujian"]?></td>					
										<td><?=$data_peserta["name"]?></td>
										<td><?=$data_peserta["prodi"]?></td>
										<td><?=$data_peserta["hasil_audisi"]?></td>
										<td><?=$data_peserta["jenis_suara"]?></td>
										<td><?=$data_peserta["nilai_akhir"]?></td>
									
										<td><a class="btn btn-danger" href="session_admin_audisi.php?noujianCari=<?=$data_peserta["noujian"]?>">Edit</a></td>
									</tr>

									<?php	
								}
								?>
								</form>

								</table>	
								</div>
							</div>
						</div>
					</div>
				</div><!--/.row-->

				<!--Lahan Edit-->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								Update Pembayaran Peserta Audisi 2020
							</div>
							
							<!--/ Form edit-->
							<div class="panel-body">
								<div class="canvas-wrapper">
								<div>
							<?php
								include('config.php');
								$noujianCari=$_GET['noujianCari'];
								$cari = mysqli_query($konek, "SELECT * FROM tbl_siswa JOIN tbl_audisi ON tbl_siswa.noujian=tbl_audisi.noujian WHERE tbl_siswa.noujian='$noujianCari' ")or die(mysqli_eror($konek));
								
								$data_cari = mysqli_fetch_object($cari);
							?>
							
							<form role="form" action="" method="post">
								
							<div class="col-md-1">	
								<div class="form-group">
										<label>NIM</label>
										<input class="form-control" name="noujianUpdate" value="<?=$data_cari->noujian?>">
									</div>
							</div>
							<div class="col-md-2">
							
									<div class="form-group">
										<label>Nama Lengkap</label>
										<input class="form-control" name="nameUpdate" value="<?=$data_cari->name?>" >
									</div>
								
							</div>		
							<div class="col-md-2">
							
									<div class="form-group">
										<label>Nilai Akhir</label>
										<input class="form-control" name="nilaiAkhir" value="<?=$data_cari->nilai_akhir?>" >
									</div>
								
							</div>							
							<div class ="col-md-2">
									<div class="form-group">
										<label>Lolos / Gagal</label>
										<select class="form-control" name="hasil_audisiUpdate" value="<?=$data_peserta->hasil_audisi?>">
											<option value="<?=$data_peserta->hasil_audisi?>"><?=$data_peserta->hasil_audisi?> (Selected) </option>
											<option value="Lolos">LOLOS</option>
											<option value="Gagal">GAGAL</option>
											<option value="Cadangan">CADANGAN</option>
										</select>
									</div>
									</div>
									<div class="col-md-2">
									<div class="form-group">
										<label>Jenis Suara</label>
										<select class="form-control" name="jenis_suaraUpdate" value="<?=$data_peserta->jenis_suara?>">
											<option value="<?=$data_peserta->jenis_suara?>"><?=$data_peserta->jenis_suara?> (Selected) </option>
											<option value="Sopran 1">SOPRAN 1</option>
											<option value="Sopran 2">SOPRAN 2</option>
											<option value="Alto 1">ALTO 1</option>
											<option value="Alto 2">ALTO 2</option>
											<option value="Tenor 1">TENOR 1</option>
											<option value="Tenor 1">TENOR 2</option>
											<option value="Bass 1 ">BASS 1</option>
											<option value="Bass 2">BASS 2</option>
										</select>
									</div>
									</div>

							<div class="col-md-2">
							
						
							</div>
							<div class="col-md-2">
									<label>Simpan</label>
									<input type="submit" class="btn btn-success" name="update_hasil"  value="Simpan Update Audisi"></td>	
							</div>
						</form>
						</div>
								</div>
							</div>
						</div>
					</div>
				</div><!--/.row-->

					
			</div><!-- /.col-->

		</div><!-- /.row -->
	</div><!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	
	<?php if(isset($_POST['update_hasil'])){
			include ('config.php');
			$hasil_audisi = $_POST['hasil_audisiUpdate'];
			$jenis_suara = $_POST['jenis_suaraUpdate'];
			$noujian = $_POST['noujianUpdate'];
			$bayar = $_POST['bayarUpdate'];

			$q_updatetblaudisi = mysqli_query($konek,"UPDATE `tbl_audisi`
			 SET `nilai_akhir`='$nilai_akhir',
			`hasil_audisi`='$hasil_audisi',
			`jenis_suara`='$jenis_suara'WHERE `noujian`='$noujian'");
									
			if($q_updatetblaudisi ){
				echo "<script>alert('Data Audisi Update Tersimpan')</script>";
				echo "<script>window.location='session_admin_audisi.php'</script>";  //biar lgsg kesimpen
												
				}else{
				echo "<script>alert('Data tidak tersimpan')</script>";
				}
		} 
	?>


</body>
</html>
